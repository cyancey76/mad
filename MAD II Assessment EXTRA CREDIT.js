/*
	https://www.thepolyglotdeveloper.com/2015/05/use-regex-to-test-password-strength-in-javascript/
	Example Js code to use Regular Expressions to check for a Strong Password
*/
var strongPasword = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{7,})");

if (strongPasword.test($elInPasswordSignUp.val())) {
		// Your existing Sign Up code goes here
} else {
	console.log("Password IS NOT valid");
	$elPopErrorSignUpWeak.popup();
	$elPopErrorSignUpWeak.popup("open", { "transition": "flip" });
} // END If...Else checking password validity
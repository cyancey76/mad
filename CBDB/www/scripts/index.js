﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in cordova-simulate or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
(function() {
    "use strict";

    document.addEventListener( 'DOMContentLoaded', DOMContentLoaded.bind( this ), false );
    document.addEventListener( 'deviceready', onDeviceReady.bind( this ), false );

    function DOMContentLoaded( event ) {
        console.log("DOM content loaded");

        var $elAllForms     = $("form");

        (function fnAddClearFieldButton() {
            $elAllForms.each( function( index ) {
                var $elInputs = $(':input[type=text], :input[type=email], :input[type=password]');
                var $elClearInputButtons = $('.clear-input-button');

                $elInputs.each( function() {
                    var $thisElement = $(this);
                    $thisElement.unbind()
                        .on( "keyup", function() {
                            if( $thisElement.val() !== "" ) {
                                $thisElement.closest(".ui-field-contain").find(".clear-input-button").removeClass("hidden");
                            } else {
                                $thisElement.closest(".ui-field-contain").find(".clear-input-button").addClass("hidden");
                            }
                        })
                        .focus( function() { 
                            if( $thisElement.val() !== "" ) {
                                setTimeout( function() {
                                    $thisElement.closest(".ui-field-contain").find(".clear-input-button").removeClass("hidden");
                                }, 10 );
                            }
                        })
                        .blur( function() { 
                            setTimeout( function() {
                                if( $thisElement.val() === "" ) {
                                    $thisElement.closest(".ui-field-contain").find(".clear-input-button").addClass("hidden");
                                }
                            }, 10 );
                        });
                });
                $elClearInputButtons.each( function( index, event ) {
                    var $thisElement = $(this),
                        $formSubmitButton = $thisElement.closest("form").find(":input[type=submit]");
                    $thisElement.unbind().click( function( event ) {
                        $thisElement.closest(".ui-field-contain").find(":input").val("").focus();
                        $thisElement.addClass("hidden");
                        $formSubmitButton.button('disable');
                    });
                });
            })
        })();

        (function fnEnableAfterRequired() {
            $elAllForms.each( function( index ) {
                var $thisForm = $(this),
                    $requiredInputs = $thisForm.find(':input[required]'),
                    $formSubmitButton = $thisForm.find(':input[type=submit]');

                $formSubmitButton.attr("disabled", true);

                $requiredInputs.each( function ( index ) {
                    var $thisElement = $(this);
                    $thisElement.on("keyup", function () {
                        var $siblingRequired = $(this).closest("form").find(":input[required]");
                        $formSubmitButton.attr("Disabled", false);
                        $siblingRequired.each(function () {
                            if ($(this).val() === "") {
                                $formSubmitButton.attr("Disabled", true).button('refresh');
                            } else {
                                $formSubmitButton.button('refresh');
                            }
                        });
                    });
                });
            })
            
        })(); // END fnEnableAfterRequired()

    }

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener( 'pause', onPause.bind( this ), false );
        document.addEventListener('resume', onResume.bind(this), false);
        // Android back button press. Doesn't work on iOS.
        document.addEventListener('backbutton', onBackKeyDown.bind(this), false);

// NOTE: Generic Variable Definitions Start
        // customize elements for logged in user
        if (localStorage.getItem("loggedIn")) {
            var uid = localStorage.getItem("loggedIn");
        }
        var $elUserButton = $(".user-menu-button");

        // forms
        var $elFormSignUp    = $("#fmSignUp"), // sign up screen form
            $elFormLogIn     = $("#fmLogIn"), // log in screen form
            $elFormSaveComic = $("#fmSaveComic"); // save comic form

        // sign up popup messages
        var $elPopErrorSignUpMismatch  = $("#popErrorSignUpMismatch"),
            $elPopErrorSignUpExists    = $("#popErrorSignUpExists"),
            $elPopErrorSignUpWeak      = $("#popErrorSignUpWeak"),
            $elPopSuccessSignUpWelcome = $("#popSuccessSignUpWelcome");

        // log in popup messages
        var $elPopSuccessLogInAttempts      = $("#popErrorLogInAttempts"),
            $elPopSuccessLogInNotExists     = $("#popErrorLogInNotExists"),
            $elPopSuccessLogInWrongPassword = $("#popErrorLogInWrongPassword");

        // logout button
        var $elLogOutActionButton = $(".logout-button");

        // Variable for deleting database
        var $elBtnDeleteCollection = $("#btnDeleteCollection");

        // save comic popup messages
        var $elSaveComicDynamic = $("#popSaveComicDynamic .content-container");

        // Variable to display our Table of comics
        var $elDivShowComicsTable = $("#divShowComicsTable");

        // Collection count spans
        var $elSpanCollectionCount = $(".collection-count");

        //PouchDB Database
        var localDB;
        // var remoteDB = new PouchDB('http://localhost:8888/CBDB');

        // Variables for edit/delete single comics
        var $elBtnDeleteComic = $("#btnDeleteComic"),
            $elBtnEditComic   = $("#btnEditComic");
        // Global-scope variable to keep track of the comic to work with
        // Note: It's undefined (not assigned anything, yet)
        // Note: Purely for naming, no $el because it's not based on HTML Element/Node
        var comicWIP;
        // Variables for the Edit Comic elements
        var $elFmEditComicsInfo = $("#fmEditComicsInfo"),
            $elBtnEditComicsCancel = $("#btnEditComicsCancel");

        // Variables for Barcode Scanner and Taking Photo
        var $elBtnBarcodeScan = $("#btnBarcodeScan"),
        $elBtnPhotoCapture = $("#btnPhotoCapture");
// NOTE: Generic Variable Definitions End

// NOTE: Function Definitions Start
        function fnClearAllFields(form) {
            console.log("fnClearAllFields() running");
            form.each(function () {
                $(this).find(":input[type=submit]").attr("disabled", true).button("refresh");
                $(this).find(':input').val("");
            });
        }

        function fnClearPasswordFields(form) {
            console.log("fnClearPasswordFields() running");
            form.each(function () {
                $(this).find(":input[type=submit]").attr("disabled", true);
                $(this).find(':input[type=password]').val("");
            });
        }

        function fnInitDB(user) {
            console.log("fnInitDB() running");
            localDB = new PouchDB( user );
            return localDB;
        } // END fnInitDB()

        function fnLogInActions( user ) {
            console.log("fnLogInActions() running");
            fnInitDB(user);
            fnRefreshAllComicsView();
            $elUserButton.html(user);
            fnClearAllFields($elFormLogIn);
            fnClearAllFields($elFormSignUp);
            $(":mobile-pagecontainer").pagecontainer("change", "#pgLoggedInHome", { "transition": "none" });
        } // END fnLogInActions()

        function fnLogOutActions() {
            console.log("fnLogOutActions() running");
            localStorage.removeItem("loggedIn");
            $elUserButton.html("User");
            $(":mobile-pagecontainer").pagecontainer("change", "#pgWelcome", { "transition": "slidedown" });
        } // END fnLogOutActions()

        function fnSignUp(event) {
            console.log("fnSignUp() running");
            event.preventDefault();

            var $elInEmailSignUp = $("#inEmailSignUp"),
                $elInPasswordSignUp = $("#inPasswordSignUp"),
                $elInPasswordConfirmSignUp = $("#inPasswordConfirmSignUp"),
                arSignUpFormValues = {
                    "email": $elInEmailSignUp.val(),
                    "password": $elInPasswordSignUp.val(),
                    "confirmpass": $elInPasswordConfirmSignUp.val()
                },
                global_users = JSON.parse(localStorage.getItem("users"));

            if (arSignUpFormValues["password"] === arSignUpFormValues["confirmpass"]) {
                var tmpValInEmailSignUp = $elInEmailSignUp.val().toLowerCase(),
                    tmpValInPasswordSignUp = $elInPasswordSignUp.val();

                if (localStorage.getItem(tmpValInEmailSignUp) === null) {
                    localStorage.setItem(tmpValInEmailSignUp, tmpValInPasswordSignUp);
                    $elPopSuccessSignUpWelcome.popup();
                    $elPopSuccessSignUpWelcome.popup("open", { "transition":"flip" });
                } else {
                    $elPopErrorSignUpExists.popup();
                    $elPopErrorSignUpExists.popup("open", { "transition":"flip" });
                }

                fnClearAllFields($elFormSignUp);
            } else {
                $elPopErrorSignUpMismatch.popup();
                $elPopErrorSignUpMismatch.popup("open", { "transition": "flip" });
                fnClearPasswordFields($elFormSignUp);
            }
        } // end fnSignUp()

        function fnLogIn(event) {
            console.log("fnLogIN() running");
            event.preventDefault();

            var $elInEmailLogIn = $("#inEmailLogIn"),
                $elInPasswordLogIn = $("#inPasswordLogIn"),
                arLogInFormValues = {
                    "email": $elInEmailLogIn.val().toLowerCase(),
                    "password": $elInPasswordLogIn.val()
                };

            // check if user exists
            if (localStorage.getItem(arLogInFormValues["email"])) {
                // user EXISTS
                // check if password is right
                if (localStorage.getItem(arLogInFormValues["email"]) === arLogInFormValues["password"]) {
                    // password RIGHT
                    localStorage.setItem("loggedIn", arLogInFormValues["email"]);
                    fnLogInActions(arLogInFormValues["email"]);
                } else {
                    // password WRONG
                    $elPopSuccessLogInWrongPassword.popup();
                    $elPopSuccessLogInWrongPassword.popup("open", { "transition": "flip" });
                    fnClearPasswordFields($elFormLogIn);
                } // end check if password is right
            } else {
                // user NO EXIST
                $elPopSuccessLogInNotExists.popup();
                $elPopSuccessLogInNotExists.popup("open", { "transition":"flip" });
            } // end check if user exists
        } // end fnLogIn()

        function fnCollectionCount( count ) {
            console.log("fnCollectionCount() running");
            if (count === 0) {
                $elSpanCollectionCount.html("0 comics");
            } else if( count === 1 ) {
                $elSpanCollectionCount.html(count + " comic");
            } else {
                $elSpanCollectionCount.html(count + " comics");
            }
        } // END fnCollectionCount()

        function fnGetFirstWord(comicName) {
            console.log("fnGetFirstWord() running");
            if (comicName.indexOf(" ") === -1) {
                return comicName;
            } else {
                return comicName.substring(0, comicName.indexOf(" "));
            } 
        } // END fnGetFirstWord

        function fnComicID(title, year, number) {
            console.log("fnComicID() running");
            var tmpID1 = fnGetFirstWord(title.toUpperCase()),
                tmpID2 = title.toUpperCase(),
                tmpID3 = "";

            switch (tmpID1) {
                case "THE":
                    tmpID3 = tmpID2.replace("THE ", "");
                    break;
                case "A":
                    tmpID3 = tmpID2.replace("A ", "");
                    break;
                case "AN":
                    tmpID3 = tmpID2.replace("AN ", "");
                    break;
                case "EL":
                    tmpID3 = tmpID2.replace("EL ", "");
                    break;
                default:
                    tmpID3 = tmpID2;
                    break;
            }

            return tmpID3.replace(/\W/g, "") + year + number;
        } // END fnComicID()

        function fnPrepComic() {
            console.log("fnPrepComic() running");
            var $valInTitle = $("#inTitle").val(),
                $valInNumber = $("#inNumber").val(),
                $valInYear = $("#inYear").val(),
                $valInPublisher = $("#inPubliser").val(),
                $valInNotes = $("#inNotes").val(),
                $valInBarcodeData = $("#inBarcodeData").val(),
                $valInPhotoData = $("#inPhotoData").val();

            var tmpID1 = fnGetFirstWord($valInTitle.toUpperCase()),
                tmpID2 = $valInTitle.toUpperCase(),
                tmpID3 = "";

            switch (tmpID1) {
                case "THE":
                    tmpID3 = tmpID2.replace("THE ", "");
                    break;
                case "A":
                    tmpID3 = tmpID2.replace("A ", "");
                    break;
                case "AN":
                    tmpID3 = tmpID2.replace("AN ", "");
                    break;
                case "EL":
                    tmpID3 = tmpID2.replace("EL ", "");
                    break;
                default:
                    tmpID3 = tmpID2;
                    break;
            }

            var tmpComic = {
                "_id": fnComicID( $valInTitle, $valInYear, $valInNumber ),
                "title": $valInTitle,
                "number": $valInNumber,
                "year": $valInYear,
                "publisher": $valInPublisher,
                "notes": $valInNotes,
                "barcode": $valInBarcodeData,
                "photo": $valInPhotoData
            }; // END JSON bundle of data

            return tmpComic;
        } // END fnPrepComic()

        function fnSaveNewComic(event) {
            console.log("fnSaveNewComic() running");
            event.preventDefault();

            var aComic = fnPrepComic();

            localDB.put(aComic)
                .then(function (response) {
                    $elSaveComicDynamic.html("Successfully saved comic!");
                    $elSaveComicDynamic.popup();
                    $elSaveComicDynamic.popup("open", { "transition": "flip" });
                    fnClearAllFields($elFormSaveComic);
                    fnRefreshAllComicsView();
                })
                .catch(function (err) {
                    switch (err.status) {
                        case 400:
                            $elSaveComicDynamic.html("Data must be in JSON format");
                            $elSaveComicDynamic.popup();
                            $elSaveComicDynamic.popup("open", { "transition": "flip" });
                            break;
                        case 409:
                            $elSaveComicDynamic.html( "You have already saved this comic!" );
                            $elSaveComicDynamic.popup();
                            $elSaveComicDynamic.popup("open", { "transition": "flip" });
                            break;
                        case 412:
                            $elSaveComicDynamic.html("_id is empty");
                            $elSaveComicDynamic.popup();
                            $elSaveComicDynamic.popup("open", { "transition": "flip" });
                            break;
                        default:
                            $elSaveComicDynamic.html("Error: " + err.status + " - " + err.message);
                            $elSaveComicDynamic.popup();
                            $elSaveComicDynamic.popup("open", { "transition": "flip" });
                            break;
                    }
                });
        } // END fnSaveNewComic(event)

        function fnRefreshAllComicsView() {
            console.log("fnRefreshAllComicsView() running");
            if (localDB) {
                localDB.allDocs({ "ascending": true, "include_docs": true }, function (failure, success) {
                    if (failure) {
                        console.log("Error retrieving comics: " + failure);
                    } else {
                        if (success.rows[0] === undefined) {
                            $elDivShowComicsTable.html("No comics yet. Save some!");
                        } else {
                            fnCollectionCount(success.rows.length);

                            var comicsTableRows = "";
                            $.each(success.rows, function (key, value) {
                                var _id = fnComicID(this.doc.title, this.doc.year, this.doc.number);
                                comicsTableRows += "<tr data-id=\"" + _id + "\"><td>" + this.doc.title + "</td><td>" + this.doc.number + "</td><td class=\"btnShowComicsInfo\">&#x1F4AC;</td></tr>";
                            });

                            var completeComicsTable = "<table id=\"all-comics-table\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> <tr> <th>Title</th> <th>Issue #</th> <th>Details</th> </tr>" + comicsTableRows + "</table > ";

                            $elDivShowComicsTable.html(completeComicsTable);
                        }
                    } // END if..else .allDocs
                }); // END .allDocs() to get all comics
            }
        } // END fnRefreshAllComicsView()

        function fnDeleteCollection() {
            console.log("fnDeleteCollection() running");
            localDB.destroy(function (failure, success) {
                if (failure) {
                    console.log("Error: " + failure);
                    window.alert("ERROR \n Failed to delete the database. Contact the developer.");
                } else {
                    window.alert("Successfully deleted the database.");
                    fnInitDB(uid);
                    $(":mobile-pagecontainer").pagecontainer("change", "#pgUser", 
                        { 
                            transition : "none",
                            role : "back",
                            changeHash: true
                        }
                    );
                } // END if..else via .destroy()
            });
        } // END fnDeleteCollection()

        function fnShowComicsInfo(thisComic) {
            console.log("fnShowComicsInfo() is running");
            console.log(thisComic);
            console.log("The _id of this <tr> is: " + thisComic.data("id"));

            var tmpComic = thisComic.data("id");

            comicWIP = tmpComic;

            localDB.get(tmpComic, function (failure, success) {
                if (failure) {
                    console.log("Error getting the comic: " + failure);
                } else {
                    console.log("Success getting the comic: " + success.title);

                    $("#divViewComics p:eq(0)").html("<strong>Title: </strong>" + success.title);
                    $("#divViewComics p:eq(1)").html("<strong>Number: </strong>" + success.number);
                    $("#divViewComics p:eq(2)").html("<strong>Year: </strong>" + success.year);
                    $("#divViewComics p:eq(3)").html("<strong>Publisher: </strong>" + success.publisher);
                    $("#divViewComics p:eq(4)").html("<strong>Notes: </strong>" + success.notes),
                    $("#divViewComics p:eq(5)").html("<strong>Barcode: </strong>" + success.barcode),
                    $("#divViewComics p:eq(6) img").attr("src", success.photo);
                } // END If..Else .get()
            }); // END .get() a comic

            $(":mobile-pagecontainer").pagecontainer("change", "#popViewComicsInfo", { "role": "dialog" });
        } // END fnShowComicsInfo()

        function fnDeleteComic() {
            console.log("fnDeleteComic() running");
            console.log("About to delete: " + comicWIP);

            localDB.get(comicWIP, function (failure, success) {
                if (failure) {
                    console.log("Comic doesn't exist: " + failure);
                } else {
                    console.log("Comic does exist. About to delete: " + success);
                    switch (window.confirm("About to delete this comic. \nAre you sure?")) {
                        case true:
                            console.log("They DO want to delete the comic");
                            localDB.remove(success, function (failure, success) {
                                if (failure) {
                                    console.log("Error in deleting: " + failure);
                                } else {
                                    console.log("Successefuly deleted: " + success);
                                    fnRefreshAllComicsView();
                                    $("#popViewComicsInfo").dialog("close");
                                } // END If..Else .remove()
                            }); // END .remove()
                            break;
                        case false:
                            console.log("They DO NOT want to delete the comic");
                            break;
                        default:
                            console.log("Unknown response");
                            break;
                    } // switch() to confirm they do want to delete
                } // END If..Else .get() to retrieve the comic about to delete
            }); // END .get() to retrieve the comic about to delete
        } // END fnDeleteComic()

        function fnEditComic() {
            console.log("fnEditComic() is running. About to edit: " + comicWIP);

            // Get the fields of the comic in question and add them to the Form
            localDB.get(comicWIP, function (failure, success) {
                if (failure) {
                    console.log("Error getting the comic: " + failure);
                } else {
                    console.log("Success getting comic: " + success);
                    // .val() can be used READ or WRITE data to a Form Field
                    $("#inTitleEdit").val(success.title);
                    $("#inNumberEdit").val(success.number);
                    $("#inYearEdit").val(success.year);
                    $("#inPublisherEdit").val(success.publisher);
                    $("#inNotesEdit").val(success.notes);
                } // END If..Else
            }); // END .get()

            $(":mobile-pagecontainer").pagecontainer("change", "#popEditComicsInfo", { "role": "dialog" });
        } // END fnEditComic()

        function fnEditComicsCancel() {
            console.log("fnEditComicsCancel() is running");
            // To-do: add vibration
            // To-do: add a sound
            $("#popEditComicsInfo").dialog("close");
        } // END fnEditComicsCancel()

        function fnEditComicsSubmit(event) {
            console.log("fnEditComicsSubmit(event) is running");
            event.preventDefault();

            // Read, again, the contents of each of the input form fields to process them
            var $valInTitleEdit = $("#inTitleEdit").val(),
                $valInNumberEdit = $("#inNumberEdit").val(),
                $valInYearEdit = $("#inYearEdit").val(),
                $valInPublisherEdit = $("#inPublisherEdit").val(),
                $valInNotesEdit = $("#inNotesEdit").val();

            console.log($valInTitleEdit, $valInNumberEdit, $valInYearEdit, $valInPublisherEdit, $valInNotesEdit);

            // First check if the comic in question exists and then update any fields
            localDB.get(comicWIP, function (failure, success) {
                if (failure) {
                    console.log("Could not find comic: " + failure);
                } else {
                    console.log("About to update comic: " + success);
                    localDB.put(
                        {
                            "_id": success._id,
                            "title": $valInTitleEdit,
                            "number": $valInNumberEdit,
                            "year": $valInYearEdit,
                            "publisher": $valInPublisherEdit,
                            "notes": $valInNotesEdit,
                            "_rev": success._rev
                        }, function (failure, success) {
                            if (failure) {
                                console.log("Error: " + failure);
                            } else {
                                console.log("Success: " + success);
                                $("#divViewComics p:eq(0)").html("<strong>Title: </strong>" + $valInTitleEdit);
                                $("#divViewComics p:eq(1)").html("<strong>Number: </strong>" + $valInNumberEdit);
                                $("#divViewComics p:eq(2)").html("<strong>Year: </strong>" + $valInYearEdit);
                                $("#divViewComics p:eq(3)").html("<strong>Publisher: </strong>" + $valInPublisherEdit);
                                $("#divViewComics p:eq(4)").html("<strong>Notes: </strong>" + $valInNotesEdit);
                                fnRefreshAllComicsView();
                                $("#popEditComicsInfo").dialog("close");
                            } // END If..Else
                        } // END .put() Failure/Success
                    ); // END .put() for updating the comic
                } // END If..Else
            }); // END .get()
        } // END fnEditComicssubmit()

        function fnBarcodeScan() {
            console.log("fnBarcodeScan() is running");

            cordova.plugins.barcodeScanner.scan(
                function (result) {
                    console.log("Type of barcode: " + result.format);
                    console.log("Data in the barcode: " + result.text);
                    $("#inBarcodeData").val(result.text); // If we just want to access something quickly
                    // $elInBarcodeData.val(success.text); // If we created a Variable first
                },
                function (error) {
                    window.alert("Scanning failure: " + error);
                },
                {
                    "prompt": "Place the barcode in the scan area",
                    "resultDisplayDuration": 2000,
                    "orientation": "landscape",
                    "disableSuccessBeep": false
                }
            ); // END .scan()
        } // END fnBarcodeScan()

        function fnPhotoCapture() {
            console.log("fnPhotoCapture() is running");

            navigator.camera.getPicture(
                function (success) {
                    console.log("Got photo: " + success);

                    $("#inPhotoData").val(success);
                },
                function (failure) {
                    window.alert("Photo fail: " + failure);
                },
                {
                    "quality": 10,
                    "saveToPhotoAlbum": true,
                    "targetWidth": 768,
                    "targetHeight": 1024
                }
            ); // END .getPicture();
        } // END fnPhotoCapture()
// NOTE: Function Definitions End

// NOTE: Events Start
        $("body").pagecontainer({
            beforeshow: function (event, ui) {
                fnRefreshAllComicsView();
            }
        });

        $elFormSignUp.submit(function( event ) {
            fnSignUp( event );
        });

        $elFormLogIn.submit(function( event ) {
            fnLogIn( event );
        });

        $elFormSaveComic.submit(function( event ) {
            fnSaveNewComic( event );
        });

        $elLogOutActionButton.on("click", function( event ) {
            event.preventDefault();
            // confirm user wants to log out
            if (window.confirm("Are you sure you want to log out?")) {
                fnLogOutActions();
            }
        });

        $elBtnDeleteCollection.on("click", fnDeleteCollection);

        $elDivShowComicsTable.on("click", ".btnShowComicsInfo", function () {
            fnShowComicsInfo($(this).parent());
        });

        // Event Listeners for Editing/Deleting a comic
        $elBtnDeleteComic.on("click", fnDeleteComic);
        $elBtnEditComic.on("click", fnEditComic);

        $elBtnEditComicsCancel.on("click", fnEditComicsCancel);
        $elFmEditComicsInfo.submit(function (event) { fnEditComicsSubmit(event); });

        $elBtnBarcodeScan.on("click", fnBarcodeScan);
        $elBtnPhotoCapture.on("click", fnPhotoCapture);
// Events end

// Do things when app starts
        if (uid) {
            fnLogInActions( uid );
        }


        window.setTimeout(function () {
            navigator.splashscreen.hide();
        }, 2000);
        // END do things when app starts
    }; // end onDeviceReady()

    function onPause() {
        // NOTE: This application has been suspended. Save application state here.
    };  // end onPause()

    function onResume() {
        // NOTE: This application has been reactivated. Restore application state here.
        if (localStorage.getItem("loggedIn")) {
            fnLogInActions(localStorage.getItem("loggedIn"));
        }
    };  // end onResume()

    function onBackKeyDown( event ) {
        event.preventDefault();
    } // end onBackKeyDown()
} )();